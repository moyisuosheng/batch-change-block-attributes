// ShowInfoWindows.cpp : 实现文件
//

#include "stdafx.h"
#include "Resource.h"
#include "ShowInfoWindows.h"
#include "afxdialogex.h"



//#include "afxeditbrowsectrl.h"


#include "CMyEdit.h"
#include "ChangeWindows.h"
#include "GridCellCombo.h"
#include "GridCellCheck.h"
#include "GridCellNumeric.h"
#include "GridCellDateTime.h"

// ShowInfoWindows 对话框
#include "ExecuteCodes.h"


#include "FilePathManage.h"
#include <iostream>
#include <string>
#include <io.h>


using namespace std;



std::list< PDbProcessingResults> pDbProcessingResultsList;

IMPLEMENT_DYNAMIC(ShowInfoWindows, CDialog)

ShowInfoWindows::ShowInfoWindows(CWnd* pParent /*=NULL*/)
	: CDialog(IDD_SHOWINFOWINDOWS, pParent)
{

}

ShowInfoWindows::~ShowInfoWindows()
{
}

void ShowInfoWindows::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GRID, m_ShowGrid);
}


BEGIN_MESSAGE_MAP(ShowInfoWindows, CDialog)
END_MESSAGE_MAP()


// ShowInfoWindows 消息处理程序

int InsertShowGridNewRow(CGridCtrl* my_CGridCtrl, int RowHeight)
{
	CString RowName;
	int RowSize = my_CGridCtrl->GetRowCount();
	RowName.Format(_T("%d"), RowSize);
	int InsRow = my_CGridCtrl->InsertRow(RowName);
	my_CGridCtrl->SetRowHeight(InsRow, RowHeight);
	//第二列
	
	my_CGridCtrl->RedrawRow(InsRow);
	return InsRow;
}

int AddDataToCtrl(CGridCtrl* my_CGridCtrl,std::list< PDbProcessingResults> pDbProcessingResultsList) {

	//行计数
	int row = 0;
	//列计数
	int col = 1;
	for (std::list<PDbProcessingResults>::iterator it = pDbProcessingResultsList.begin(); it != pDbProcessingResultsList.end(); ++it) {


		PDbProcessingResults pDbProcessingResults = *it;
		
	
		
		col = 1;
		for (std::list<AttributeStruct>::iterator itAttributeStruct = pDbProcessingResults.attributeList.begin(); itAttributeStruct != pDbProcessingResults.attributeList.end(); ++itAttributeStruct) {

			AttributeStruct attributeStruct = *itAttributeStruct;
			row++;
		//	infoStr += "文件：" + pDbProcessingResults.FileFullName + " 描述：" + attributeStruct.Info +" 保存路径：" + pDbProcessingResults.FileSavePath + " 读取路径：" + pDbProcessingResults.FileReadPath + "\r\n";
			//赋值行的每一列单元内容

			//添加行
		
			
			if (attributeStruct.ProcessingResults == EnumProcessingResults::Ok) {
		
				for (int i = 1; i < 5; i++)
				{
					/*
					{
					GV_ITEM Item;
					Item.row = row;//行
					Item.col = i;//列
					Item.mask |= GVIF_BKCLR;//背景色 GVIF_FGCLR(前景色)
					Item.crBkClr = GetSysColor(COLOR_3DFACE);//自已定
					my_CGridCtrl->SetItem(&Item);
					}
					*/
					//成功的单元格显示绿色193 255 193
					my_CGridCtrl->SetItemBkColour(row, i, RGB(193, 255, 193));
				}
					
				}
			else
			{
				for (int i = 1; i < 5; i++)
				{
					//其他的的单元格显示红色 255 240 245
					my_CGridCtrl->SetItemBkColour(row, i, RGB(255, 240, 245));
				}
				
			}
			

			my_CGridCtrl->SetItemText(row, 1, stringTransformationCString(pDbProcessingResults.FileFullName));
			my_CGridCtrl->SetItemText(row, 2, stringTransformationCString(attributeStruct.Info));
			my_CGridCtrl->SetItemText(row, 3, stringTransformationCString(pDbProcessingResults.FileReadPath));
			my_CGridCtrl->SetItemText(row, 4, stringTransformationCString(pDbProcessingResults.FileSavePath));

			InsertShowGridNewRow(my_CGridCtrl, 30);
		}





	}


	return 0;
}

BOOL ShowInfoWindows::OnInitDialog()
{
	CDialog::OnInitDialog();



	CRect cr;
	//获得画图区域
	m_ShowGrid.GetClientRect(&cr);
	int nRowNum = 1;//设置1行
	int nColNum = 5;//设置7列
	m_ShowGrid.SetColumnCount(nColNum);//设置列
	m_ShowGrid.SetRowCount(nRowNum);
	m_ShowGrid.SetFixedRowCount(1);
	m_ShowGrid.SetFixedColumnCount(1);//表头一行一列
	m_ShowGrid.SetRowHeight(1, 30);//设置行高

	//设置列宽
	m_ShowGrid.SetColumnWidth(0,50);
	m_ShowGrid.SetColumnWidth(1, 200);
	m_ShowGrid.SetColumnWidth(2, 200);
	m_ShowGrid.SetColumnWidth(3, 300);
	m_ShowGrid.SetColumnWidth(4, 300);

	m_ShowGrid.SetItemText(0, 0, _T("序号"));//设置内容
	m_ShowGrid.SetItemText(0, 1, _T("文件名"));//设置内容
	m_ShowGrid.SetItemText(0, 2, _T("描述"));//设置内容
	m_ShowGrid.SetItemText(0, 3, _T("读取路径"));//设置内容
	m_ShowGrid.SetItemText(0, 4, _T("保存路径"));//设置内容
										  //COLORREF clr = RGB(255, 255, 255);
	m_ShowGrid.SetFixedTextColor(RGB(0, 0, 0));//设置固定区域文字的颜色
	m_ShowGrid.SetFixedBkColor(RGB(238, 238, 238));//设置固定行的背景颜色
	m_ShowGrid.SetTextBkColor(RGB(255, 255, 255));//设置可编辑区域背景颜色
	m_ShowGrid.SetTextColor(RGB(0, 0, 0));//设置可编辑区域文字颜色
	m_ShowGrid.SetEditable(TRUE);//所有可编辑
	InsertShowGridNewRow(&m_ShowGrid, 30);


	//AfxMessageBox(stringTransformationCString(PDbProcessingResultsListToString(pDbProcessingResultsList)));
	AddDataToCtrl(&m_ShowGrid, pDbProcessingResultsList);
	// TODO:  在此添加额外的初始化


	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}





void ShowInfoWindows::OnFinalRelease()
{
	// TODO: 在此添加专用代码和/或调用基类

	CDialog::OnFinalRelease();
}
