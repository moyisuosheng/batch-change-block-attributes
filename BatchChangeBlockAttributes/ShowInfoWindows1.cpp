// ShowInfoWindows.cpp : 实现文件
//

#include "stdafx.h"
#include "ShowInfoWindows.h"
#include "ChangeWindows.h"
#include "afxdialogex.h"

#include "Resource.h"
//#include "afxeditbrowsectrl.h"


#include "GridCellCombo.h"
#include "GridCellCheck.h"
#include "GridCellNumeric.h"
#include "GridCellDateTime.h"

// ChangeWindows 对话框
#include "ExecuteCodes.h"


#include "FilePathManage.h"
#include <iostream>
#include <string>
#include <io.h>


using namespace std;

std::list< PDbProcessingResults> pDbProcessingResultsList;

// ShowInfoWindows 对话框

IMPLEMENT_DYNAMIC(ShowInfoWindows, CDialog)

ShowInfoWindows::ShowInfoWindows(CWnd* pParent /*=NULL*/)
	: CDialog(IDD_SHOWINFOWINDOWS, pParent)
{

}

ShowInfoWindows::~ShowInfoWindows()
{
}

void ShowInfoWindows::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	
//	DDX_Control(pDX, IDC_SHOWINFOWINDOWS, m_ShowGrid);

}


BEGIN_MESSAGE_MAP(ShowInfoWindows, CDialog)
	ON_BN_CLICKED(IDOK, &ShowInfoWindows::OnBnClickedOk)
END_MESSAGE_MAP()


// ShowInfoWindows 消息处理程序


void ShowInfoWindows::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialog::OnOK();

	AfxMessageBox(stringTransformationCString(PDbProcessingResultsListToString(pDbProcessingResultsList)));
}


BOOL ShowInfoWindows::OnInitDialog()
{
	/*

	CDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化
	CRect cr;
	//获得画图区域
	m_ShowGrid.GetClientRect(&cr);
	int nRowNum = 1;//设置1行
	int nColNum = 4;//设置4列
	m_ShowGrid.SetColumnCount(nColNum);//设置列
	m_ShowGrid.SetRowCount(nRowNum);
	m_ShowGrid.SetFixedRowCount(1);
	m_ShowGrid.SetFixedColumnCount(1);//表头一行一列
	m_ShowGrid.SetRowHeight(1, 30);//设置行高
	m_ShowGrid.SetItemText(0, 0, _T("序号"));//设置内容
	m_ShowGrid.SetItemText(0, 1, _T("类型"));//设置内容
	m_ShowGrid.SetItemText(0, 2, _T("属性"));//设置内容
	m_ShowGrid.SetItemText(0, 3, _T("值"));//设置内容

	//COLORREF clr = RGB(255, 255, 255);
	m_ShowGrid.SetFixedTextColor(RGB(0, 0, 0));//设置固定区域文字的颜色
	m_ShowGrid.SetFixedBkColor(RGB(238, 238, 238));//设置固定行的背景颜色
	m_ShowGrid.SetTextBkColor(RGB(255, 255, 255));//设置可编辑区域背景颜色
	m_ShowGrid.SetTextColor(RGB(0, 0, 0));//设置可编辑区域文字颜色
	m_ShowGrid.SetEditable(TRUE);//所有可编辑
	InsertMyNewRow(&m_ShowGrid, 30);

	*/
	
	



	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}


void ShowInfoWindows::OnFinalRelease()
{
	// TODO: 在此添加专用代码和/或调用基类

	CDialog::OnFinalRelease();
}
