#include "stdafx.h"
#include <string>
#include "DataTable.h"
DataTable::DataTable():tableName(string()) 
{
	InitData();
}

DataTable::DataTable(const string& _tableName):tableName(_tableName)
{
	InitData();
}

DataTable::~DataTable()
{
	Clear();
}

inline void DataTable::InitData()
{
	drCollect=new DataRowCollection(this);
	dcCollect=new DataColumnCollection(this);
}

DataColumnCollection& DataTable::Columns()
{
	return *dcCollect;
}

DataRowCollection& DataTable::Rows() 
{
	return *drCollect;
}

string DataTable::TableName()
{
	return tableName;
}

void DataTable::TableName(const string& _tableName)
{
	tableName=_tableName;
}

void DataTable::Clear()
{
	drCollect->Clear();
	dcCollect->Clear(); 
}
 
DataRow* DataTable::NewRow()
{
	DataRow* dr=new DataRow();
	dcCollect->CopyTo(dr->columns,0);
	dr->table=this;
	return dr;
}

//这个方法需要手动释放内存
vector<DataRow*>* DataTable::Select(const string& columnName,const string& value)
{
	vector<DataRow*>* drList=new vector<DataRow*>();
	int index=dcCollect->IndexOf(columnName);
	if(index<0)
	{
		return drList;
	}
	map<string,DataColumn*>::iterator mapIter=dcCollect->nameList.find(columnName);
	if(mapIter!=dcCollect->nameList.end())
	{
		DataColumn* dc= (*mapIter).second;
		if(dc!=NULL)
		{
			size_t size=drCollect->Count();
			for(size_t i=0;i<size;i++)
			{
				DataRow* dr=&(*drCollect)[i];
				DataColumn& tmpDc=(*dr)[dc->columnName];
				if(tmpDc.value.compare(value)==0)
				{
					drList->push_back(dr);
				}
			}
		}
	}
	return drList;
}

DataRow& DataTable::operator[](size_t index)
{
	assert(index>=0&& index<drCollect->Count());
	return (*drCollect)[index];
}