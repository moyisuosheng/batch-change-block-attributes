#include "stdafx.h"
#include <string>
#include "DataTable.h"

DataColumnCollection::DataColumnCollection(DataTable* _table):table(_table)
{ 
	InitData();
}

DataColumnCollection::DataColumnCollection():table(NULL)
{
    InitData();
}

inline void DataColumnCollection::InitData()
{
	dlist=vector<DataColumn*>();
	nameList=map<string,DataColumn*>();
}

inline void DataColumnCollection::Add(DataColumn* dc)
{
	Add(dc,dlist.size()); 
}

void DataColumnCollection::Add(DataColumn* dc,size_t index)
{
    if(dc==NULL)
		return ;
	if(dc->table!=NULL && dc->table==this->table)
		return;
	assert(index>=0 && index<=dlist.size()); 
	dc->table=table;
	Add(dc,index,this);
	if(table!=NULL && table->drCollect->Count()>0)
	{ 
		size_t count=table->drCollect->Count();
		for(size_t i=0;i<count;i++)
		{
			DataColumn* tmpDc=new DataColumn(*dc);
			Add(tmpDc,index,(*table->drCollect)[i].columns); 
		}
	}
}

void DataColumnCollection::Add(DataColumn* dc,size_t index,DataColumnCollection* collect)
{ 
	collect->dlist.insert(collect->dlist.begin()+index,dc);
	collect->nameList.insert(std::pair<string, DataColumn*>(dc->columnName,dc));
}

DataColumn& DataColumnCollection::Add(const string& columnName)
{
	DataColumn* dc=new DataColumn(columnName);
	Add(dc);
	return *dc;
}

bool DataColumnCollection::Contains(const string& columnName)
{ 
	return nameList.find(columnName)!=nameList.end();
}

void DataColumnCollection::Clear()
{
	nameList.clear();
	for(size_t i=dlist.size()-1;i>=0;i--)
	{
		delete dlist[i];
	}
	dlist.clear();
}

int DataColumnCollection::IndexOf(const string& columnName)
{ 
	map<string,DataColumn*>::iterator mapIter=nameList.find(columnName);
	if(mapIter!=nameList.end())
	{
		vector<DataColumn*>::iterator listIter=dlist.begin();
		size_t size=dlist.size();
		for(size_t i=0;i<size;i++)
		{
			 if(*listIter==(*mapIter).second)
			 {
				return (int)i;
			 }
			 listIter=dlist.begin()+i;
		}
	} 
	return -1;
}

void DataColumnCollection::Remove(DataColumn& dc)
{
	Remove(dc.columnName);
}
 
void DataColumnCollection::Remove(const string& columnName)
{
	int hashCode=StringHelper::HashCode(columnName);
	map<string,DataColumn*>::iterator mapIter=nameList.find(columnName);
	if(mapIter!=nameList.end())
	{
		vector<DataColumn*>::iterator listIter=dlist.begin();
		size_t index=0;
		while(listIter!=dlist.end())
		{
			if(*listIter++==(*mapIter).second)
			{
				break;
			} 
			index++;
		} 
		RemoveAt(index);
	}  
}

void DataColumnCollection::RemoveAt(size_t index)
{
	assert(index>=0 && index<=dlist.size()); 
	vector<DataColumn*>::iterator listIter=dlist.begin()+index; 
	map<string,DataColumn*>::iterator mapIter=nameList.find((*listIter)->columnName); 
	
	if(table!=NULL && table->drCollect->Count()>0)
	{ 
		size_t count=table->drCollect->Count();
		for(size_t i=0;i<count;i++)
		{
			RemoveAt(index,(*table->drCollect)[i].columns);
		}
	}
	 
	if(mapIter!=nameList.end())
	{
		 nameList.erase(mapIter);
	}
	delete *listIter;
	dlist.erase(listIter); 
}

void DataColumnCollection::RemoveAt(size_t index,DataColumnCollection* collect)
{
	vector<DataColumn*>::iterator iter=collect->dlist.begin()+index;
	if(iter!=collect->dlist.end())
	{
		map<string,DataColumn*>::iterator mapIter=collect->nameList.find((*iter)->columnName);
		if(mapIter!=collect->nameList.end())
		{
			collect->nameList.erase(mapIter) ;
		}
		delete *iter;
		collect->dlist.erase(iter); 
	}
}

void DataColumnCollection::CopyTo(DataColumnCollection* arr, size_t index)
{
	assert(index>=0 && index<dlist.size());
	while(index<dlist.size()){ 
		arr->Add(new DataColumn(*dlist[index++]));
	}
}

DataColumn& DataColumnCollection::operator[](size_t index)
{
	assert(index>=0 && index<=dlist.size()); 
	return *dlist[index];
}

DataColumn& DataColumnCollection::operator[](const string& columnName)
{ 
	map<string,DataColumn*>::iterator mapIter=nameList.find(columnName);
	assert(mapIter!=nameList.end());
	return *(*mapIter).second;
}

size_t DataColumnCollection::Count()
{
	return dlist.size();
}

inline void DataColumnCollection::Table(DataTable* tb)
{
	table=tb;
}
inline DataTable* DataColumnCollection::Table()
{
	return table;
}
 
DataColumnCollection::~DataColumnCollection()
{
	Clear();
}