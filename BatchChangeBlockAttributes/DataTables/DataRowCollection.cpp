#include "stdafx.h"
#include <string>
#include "DataTable.h"
DataRowCollection::DataRowCollection(DataTable* _table)
{
	table=_table;
}

void DataRowCollection::Add(DataRow* dc)
{
	drlist.push_back(dc);
}

void DataRowCollection::Clear()
{
	for(size_t i=drlist.size()-1;i>=0;i--)
	{
		delete drlist.back(); 
		drlist.pop_back();
	}
	drlist.clear();
}

size_t DataRowCollection::Count()
{
	return drlist.size();
}

void DataRowCollection::Remove(DataRow* dr)
{
	list<DataRow*>::iterator iter=drlist.begin();
	while(iter!=drlist.end())
	{
		if(dr==*iter)
		{
			delete *iter;
			drlist.erase(iter);
			break;
		}
		iter++;
	}
}

void DataRowCollection::RemoveAt(size_t index)
{
	assert(index>=0 && index<drlist.size());
	list<DataRow*>::iterator iter=drlist.begin() ;
	while(index-->0)
	{
		iter++;
	}
	Remove(*iter);
}

DataRow& DataRowCollection::operator[](size_t index)
{
	assert(index>=0 && index<drlist.size());
	list<DataRow*>::iterator iter=drlist.begin();
	while(index-->0)
	{
		iter++;
	}
	return **iter;
}
    
inline void DataRowCollection::Table(DataTable* tb)
{
	table=tb;
}

inline DataTable* DataRowCollection::Table()
{
	return table;
}

DataRowCollection::~DataRowCollection()
{
	Clear();
}

DataRowCollection::DataRowCollection():drlist(list<DataRow*>())
{

}