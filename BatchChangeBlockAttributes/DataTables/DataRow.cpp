#include "stdafx.h"
#include <string>
#include "DataTable.h"
volatile int DataRow::rowIdCount=0;

void DataRow::Table(DataTable* tb)
{
	table=tb;
}

DataTable* DataRow::Table()
{
	return table;
}

DataColumn& DataRow::operator[](size_t index)
{ 
	return (*columns)[index];
}
DataColumn& DataRow::operator[](const string& columnName)
{
	return (*columns)[columnName];
}

DataRow::~DataRow()
{
	delete columns; 
}

DataRow::DataRow()
{
	 columns=new DataColumnCollection();
	 rowId=++rowIdCount;
}

bool DataRow::operator=(const DataRow& dr)
{
	return rowId==dr.rowId;
}

void DataRow::RemoveAt(size_t index)
{ 
	columns->RemoveAt(index);
}

void DataRow::Remove(DataColumn& dc)
{
	columns->Remove(dc);
}

DataColumnCollection& DataRow::Columns()
{
	return *columns;
}