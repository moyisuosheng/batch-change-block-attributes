#pragma once
#include "stdafx.h"
#include <string>
#include <iostream>
using namespace std; 
#include <assert.h> 
#include <vector> 
#include <list>
#include <map>
#include "DataColumn.h" 
#include "StringHelper.h"

class DataColumnCollection;
class DataRow;
class DataRowCollection;
class DataTable;

class DataColumnCollection
{
public:
	 friend class DataTable;
	 friend class DataRow;
	 friend class DataRowCollection;
	 DataColumnCollection(DataTable* table);
	 ~DataColumnCollection();
	 void Add(DataColumn* dc);
	 void Add(DataColumn* dc,size_t index);
	 
	 DataColumn& Add(const string& columnName);

     bool Contains(const string& columnName);
	 void Clear();
	 size_t Count(); 
     int IndexOf(const string& columnName);
	  
	 void Remove(DataColumn& dc);
	 void Remove(const string& columnName);
     void RemoveAt(size_t index);

	 void CopyTo(DataColumnCollection* arr, size_t index);

	 DataColumn& operator[](size_t index);
	 DataColumn& operator[](const string& columnName);
	  
	 void Table(DataTable* tb);
	 DataTable* Table();
private:
	void Add(DataColumn* dc,size_t index,DataColumnCollection* collect);
	void RemoveAt(size_t index,DataColumnCollection* collect);
	void InitData();
	DataColumnCollection(); 
	vector<DataColumn*> dlist;
	map<string,DataColumn*> nameList;
	DataTable* table;
};

class DataRow
{
public:
	friend class DataTable;
	friend class DataRowCollection;
	friend class DataColumnCollection;
	~DataRow();
	void Table(DataTable* tb); 
	DataTable* Table();

	DataColumnCollection& Columns();
	DataColumn& operator[](size_t index);
	DataColumn& operator[](const string& columnName);
	bool operator=(const DataRow& dr);
private:
	DataRow();
	void Remove(DataColumn& dc);
	void RemoveAt(size_t index); 
	DataColumnCollection* columns;
	DataTable* table;	
	int rowId;
	static volatile int rowIdCount;
};

class DataRowCollection
{
public:
	 friend class DataTable;
	 friend class DataColumnCollection;
	 DataRowCollection(DataTable* table);
	 ~DataRowCollection();
	 void Add(DataRow* dc);  
	 void Clear();
	 size_t Count();  
	 void Remove(DataRow* dc); 
     void RemoveAt(size_t index); 
	 DataRow& operator[](size_t index); 
	 void Table(DataTable* tb);
	 DataTable* Table();
private:
	DataRowCollection(); 
	list<DataRow*> drlist; 
	DataTable* table;
};


class DataTable
{
public:
	friend class DataColumnCollection;
	friend class DataRowCollection;
	DataTable();
	DataTable(const string& _tableName);
	~DataTable();

	DataColumnCollection& Columns();
	DataRowCollection& Rows();

	string TableName();
	void TableName(const string& _tableName);
	DataRow& operator[](size_t index);
	void Clear(); 
	DataRow* NewRow();
	vector<DataRow*>* Select(const string& columnName,const string& value) ;
private:
	void InitData();
	DataColumnCollection* dcCollect;
	DataRowCollection* drCollect;
	string tableName;
};