#pragma once
class DataTable;
#include "stdafx.h"
#include <string>
#include <iostream>
using namespace std; 

template<typename T>
class TDataColumn
{
public:
	TDataColumn():columnName(string()),caption(string())
	{
		 
	}

	TDataColumn(const string& _columnName):columnName(_columnName),caption(string())
	{
		 
	}
	 
	virtual ~TDataColumn()
	{
	
	}

	void Caption(const string& _caption)
	{
		caption=_caption;
	}
	string Caption()
	{
		return caption.size() ?caption:columnName; 
	}

	void  ColumnName(const string& _columnName)
	{
		columnName=_columnName;
	}
	string ColumnName()
	{
		return columnName;
	}

	void Value(const T& _value)
	{
		value=_value;
	}
	T Value()
	{
		return value;
	}
  
	int ObjectId()
	{
		return objectId;
	}

	void Table(DataTable* tb)
	{
		table=tb;
	}

	DataTable* Table()
	{ 
		return table;
	}

	string TypeName()
	{
		return typeid(T).name();
	}
	 
protected: 
	int objectId; 
	string columnName;
	string caption;
	T value;
	DataTable* table;
}; 
 

class DataColumn:public TDataColumn<string>
{
public:
	friend class DataColumnCollection;
	friend class DataTable;
	DataColumn():TDataColumn<string>() 
	{
		 objectId=++objectCountId;
	}

	virtual ~DataColumn()
	{
	
	}

	DataColumn(const DataColumn& dc)
	{
		*this=dc; 
	}

	DataColumn& operator=(const DataColumn& dc)
	{
		objectId=++objectCountId;
		columnName=dc.columnName;
		caption=dc.caption;
		value=dc.value;
		table=dc.table;
		return *this;
	}

	DataColumn(const string& _columnName):TDataColumn<string>(_columnName)
	{
		 objectId=++objectCountId;
	}

	bool operator==(const DataColumn& dc)
	{
		return objectId==dc.objectId;
	}

	bool operator!=(const DataColumn& dc)
	{
		return objectId!=dc.objectId;
	}

	bool operator<(const DataColumn& dc)
	{
		return objectId<dc.objectId;
	}

	bool operator>(const DataColumn& dc)
	{
		return objectId>dc.objectId;
	}

	void Show()
	{
		cout<<"columnName:"<<columnName.data()<<",caption:"<<Caption().c_str()<<",value:"<<value.data()<<endl;
	}
   
private: 
	static volatile int objectCountId;
}; 

