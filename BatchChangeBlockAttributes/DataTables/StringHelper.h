//StringHelper.h
//作者：陈太汉
//博客：http://www.cnblogs.com/hlxs/
#ifndef _STRING_HELPER_
#define _STRING_HELPER_
#include "stdafx.h"
#include <string>
#include <iostream>  
using namespace std;
#include <vector>
class StringHelper
{
public:
	const static string ToLower(const string& str)//转化成小写
	{
		return ToLower(str.data());
	}

	const static string ToLower(const char* str)//转化成小写
	{
		string val; 
		size_t size=strlen(str);
		for(int i=0;i<size;i++) 
		{
			val.push_back(tolower(*(str+i)));
		}
		return val;
	}

	static void Split(const char* str,const char c,vector<string>& vect)//将字符串拆分
	{ 
		size_t size=strlen(str);
		size_t start=0 ;
		for(int i=0;i<size;i++)
		{
			if(str[i]==c)
			{
				vect.push_back(string(str+start,i-start));
				start=i+1;
			}
		}
		if(start<size)
		{
			vect.push_back(string(str+start));
		}
	}

	static void Split(const string& str,const char c,vector<string>& vect)//将字符串拆分
	{ 
		return Split(str.data(),c,vect);
	}

	static int HashCode(const string& str)
	{ 
		return HashCode(str.data());
	}

	static int HashCode(const char* str,int stringComparison=0)
	{ 
		register unsigned int h=0;  
		if(stringComparison==Ordinal)
		{
			for(const char *p =str; *p ; p++) 
			{ 
				h = 31 * h + *p;  
			}  
		}else{
			for(const char *p =str; *p ; p++) 
			{ 
				h = 31 * h + tolower(*p);  
			}  
		}
		return h % 4194967295 ; 
	}

	enum StringComparison
	{
		Ordinal=0,//区分大小写
		OrdinalIgnoreCase=1//不区分大小写
	};

};

#endif