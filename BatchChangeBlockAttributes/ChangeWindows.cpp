// ChangeWindows.cpp : 实现文件
//

#include "stdafx.h"
#include "Resource.h"
//#include "afxeditbrowsectrl.h"


#include "CMyEdit.h"
#include "ChangeWindows.h"
#include "GridCellCombo.h"
#include "GridCellCheck.h"
#include "GridCellNumeric.h"
#include "GridCellDateTime.h"

// ChangeWindows 对话框
#include "ExecuteCodes.h"


#include "FilePathManage.h"
#include <iostream>
#include <string>
#include <io.h>


using namespace std;


IMPLEMENT_DYNAMIC(ChangeWindows, CDialog)


ChangeWindows::ChangeWindows(CWnd* pParent /*=NULL*/)
	: CDialog(IDD_CHANGEWINDOWS, pParent)
	, m_ReadFolderStr(_T(""))
{

	EnableAutomation();
	
}

ChangeWindows::~ChangeWindows()
{
}

void ChangeWindows::OnFinalRelease()
{
	// 释放了对自动化对象的最后一个引用后，将调用
	// OnFinalRelease。  基类将自动
	// 删除该对象。  在调用该基类之前，请添加您的
	// 对象所需的附加清理代码。

	CDialog::OnFinalRelease();
}

void ChangeWindows::DoDataExchange(CDataExchange* pDX)
{

	DDX_Control(pDX, IDC_ReadEDIT, m_ReadEdit);
	DDX_Control(pDX, IDC_WriteEDIT, m_writeEdit);

	DDX_Control(pDX, IDC_GRID, m_DataGrid);
	DDX_Control(pDX, IDC_ShowPROGRESS, m_progress);
}


BEGIN_MESSAGE_MAP(ChangeWindows, CDialog)
	ON_BN_CLICKED(IDC_ReadFolderBUTTON, &ChangeWindows::OnBnClickedReadfolderbutton)
	ON_WM_DROPFILES()
	ON_NOTIFY(GVN_ENDLABELEDIT, IDC_GRID, OnGridEndEdit)
	ON_BN_CLICKED(IDC_BUTTON1, &ChangeWindows::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_WriteFolderBUTTON, &ChangeWindows::OnBnClickedWritefolderbutton)
	ON_BN_CLICKED(IDC_BUTTON2, &ChangeWindows::OnBnClickedButton2)
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(ChangeWindows, CDialog)
END_DISPATCH_MAP()

// 注意: 我们添加 IID_IhangeWindows 支持
//  以支持来自 VBA 的类型安全绑定。  此 IID 必须同附加到 .IDL 文件中的
//  调度接口的 GUID 匹配。

// {534C9FC5-7853-4983-A472-C8BD49E9CA39}
static const IID IID_IhangeWindows =
{ 0x534C9FC5, 0x7853, 0x4983, { 0xA4, 0x72, 0xC8, 0xBD, 0x49, 0xE9, 0xCA, 0x39 } };

BEGIN_INTERFACE_MAP(ChangeWindows, CDialog)
	INTERFACE_PART(ChangeWindows, IID_IhangeWindows, Dispatch)
END_INTERFACE_MAP()





// ChangeWindows 消息处理程序

CString FicowGetDirectory()
{
	BROWSEINFO bi;
	char name[MAX_PATH];
	ZeroMemory(&bi, sizeof(BROWSEINFO));
	bi.hwndOwner = AfxGetMainWnd()->GetSafeHwnd();
	bi.pszDisplayName = (LPWSTR)name;
	bi.lpszTitle = _T("选择文件夹目录");
	bi.ulFlags = BIF_RETURNFSANCESTORS|BIF_EDITBOX| BIF_RETURNONLYFSDIRS| BIF_VALIDATE;
	LPITEMIDLIST idl = SHBrowseForFolder(&bi);
	if (idl == NULL)
		return "";
	CString strDirectoryPath;
	SHGetPathFromIDList(idl, strDirectoryPath.GetBuffer(MAX_PATH));
	strDirectoryPath.ReleaseBuffer();
	if (strDirectoryPath.IsEmpty())
		return "";
	if (strDirectoryPath.Right(1) != "\\")
		strDirectoryPath += "\\";

	CoTaskMemFree(idl); //释放pIDList所指向内存空间;

	return strDirectoryPath;
}

//CString FicowGetDirectory()
//{
//	// TODO: 在此添加命令处理程序代码
//	BROWSEINFO bi;
//	wchar_t Buffer[MAX_PATH];
//
//	//初始化入口参数 bi
//	bi.hwndOwner = AfxGetMainWnd()->GetSafeHwnd();
//	bi.pidlRoot = NULL;
//	bi.pszDisplayName = Buffer;
//	bi.lpszTitle = L"选择文件夹目录";
//	bi.ulFlags = BIF_EDITBOX;
//	bi.lpfn = NULL;
//	//bi.iImage = IDR_MAINFRAME;
//
//	LPITEMIDLIST pIDList = SHBrowseForFolder(&bi); //调用显示选择对话框 
//												   //注意下 这个函数会分配内存 但不会释放 需要手动释放
//
//	CString GamePath="";
//	if (pIDList)
//	{
//		SHGetPathFromIDList(pIDList, Buffer);
//		
//		GamePath = Buffer; //将文件夹路径保存在CString 对象里面
//		//取得文件夹路径放置Buffer空间
//		//GUI_ShowMessage(true, Buffer);
//
//	}
//	
//	CoTaskMemFree(pIDList); //释放pIDList所指向内存空间;
//	TRACE("%d", pIDList);
//
//	// 把变量内容更新到对话框
//	//UpdateData(FALSE);
//
//	return GamePath;
//}






CString openFolder()
{
	// TODO: Add your control notification handler code here
	CString filter;
	//filter = "文本文档(*.txt)|*.txt|PDF文档(*.pdf)|*.pdf||";
//	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, filter);
	CFileDialog dlg(false, NULL, NULL, OFN_HIDEREADONLY, filter);
	if (dlg.DoModal() == IDOK)
	{
		CString str;
		str = dlg.GetPathName();
		AfxMessageBox(str);
		return str;
	}
	else
	{
		return "";
	}
	
}


void ChangeWindows::OnBnClickedReadfolderbutton()
{
	SetDlgItemText(IDC_ReadEDIT, FicowGetDirectory());
}



void ChangeWindows::OnDropFiles(HDROP hDropInfo)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	
	CDialog::OnDropFiles(hDropInfo);

}


BOOL ChangeWindows::OnInitDialog()
{
	CDialog::OnInitDialog();

	
	// TODO:  在此添加额外的初始化

	CRect cr;
	//获得画图区域
	m_DataGrid.GetClientRect(&cr);
	int nRowNum = 1;//设置1行
	int nColNum = 4;//设置4列
	m_DataGrid.SetColumnCount(nColNum);//设置列
	m_DataGrid.SetRowCount(nRowNum);
	m_DataGrid.SetFixedRowCount(1);
	m_DataGrid.SetFixedColumnCount(1);//表头一行一列
	m_DataGrid.SetRowHeight(1, 30);//设置行高
	m_DataGrid.SetItemText(0, 0, _T("序号"));//设置内容
	m_DataGrid.SetItemText(0, 1, _T("类型"));//设置内容
	m_DataGrid.SetItemText(0, 2, _T("属性"));//设置内容
	m_DataGrid.SetItemText(0, 3, _T("值"));//设置内容

	//COLORREF clr = RGB(255, 255, 255);
	m_DataGrid.SetFixedTextColor(RGB(0, 0, 0));//设置固定区域文字的颜色
	m_DataGrid.SetFixedBkColor(RGB(238, 238, 238));//设置固定行的背景颜色
	m_DataGrid.SetTextBkColor(RGB(255, 255, 255));//设置可编辑区域背景颜色
	m_DataGrid.SetTextColor(RGB(0, 0, 0));//设置可编辑区域文字颜色
	m_DataGrid.SetEditable(TRUE);//所有可编辑
	InsertMyNewRow(&m_DataGrid,30);
	m_progress.SetRange(0, 100);
	m_progress.SetPos(0);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}





int InsertMyNewRow(CGridCtrl* my_CGridCtrl,int RowHeight)
{
	CString RowName;
	int RowSize = my_CGridCtrl->GetRowCount();
	RowName.Format(_T("%d"), RowSize);
	int InsRow = my_CGridCtrl->InsertRow(RowName);
	my_CGridCtrl->SetRowHeight(InsRow, RowHeight);
	my_CGridCtrl->SetCellType(InsRow, 1, RUNTIME_CLASS(CGridCellCombo));
	CGridCellCombo *pCell = (CGridCellCombo*)my_CGridCtrl->GetCell(InsRow, 1);
	CStringArray OptionsType;
	OptionsType.Add(_T("标题栏"));
	OptionsType.Add(_T("明细表"));
	OptionsType.Add(_T("自定义块"));
	pCell->SetOptions(OptionsType);   //添加单元格的下拉列表值
	pCell->SetStyle(CBS_DROPDOWNLIST);
	pCell->SetText(OptionsType.GetAt(0));
	my_CGridCtrl->RedrawRow(InsRow);
	return InsRow;
}



void ChangeWindows::OnGridEndEdit(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*)pNotifyStruct;
	int row = (pItem->iRow) - 1;        // 点击的行号(实测要减1）
	int col = (pItem->iColumn) - 1;    // 点击的列号(实测要减1）

									   //根据行号列号获取单元格内容
	int RowSize = m_DataGrid.GetRowCount();
	if (row == RowSize-2)
	{
	
		InsertMyNewRow(&m_DataGrid,30);
	}
	//CString RowName;
	//RowName.Format(_T("选中第%d行，总共%d行"), row, RowSize);
	//MessageBox(RowName);
//	CString newName = m_DataGrid.GetItemText(col, row);
	//...

}


//DataTable GetGridDataTable(CGridCtrl* my_CGridCtrl)
//{
//	DataTable dt;
//	for (int i = 0; i<my_CGridCtrl->GetColumnCount(); i++)
//	{
//
//		
//
//		DataColumn *dc = new DataColumn(GetCellValue(my_CGridCtrl, 0, i));
//		dt.Columns().Add(dc);
//	}
//
//	for (int j = 0; j<my_CGridCtrl->GetRowCount()-1; j++)
//	{
//		DataRow* dr = dt.NewRow();
//		
//		for (int i = 0; i<dt.Columns().Count(); i++)
//		{
//			
//			(*dr)[i].Value(GetCellValue(my_CGridCtrl, j, i));
//		}
//		dt.Rows().Add(dr);
//	}
//
//	return dt;
//}



void ChangeWindows::OnBnClickedButton1()
{

	CString InputCStr;
	CString OutputCStr;
	m_ReadEdit.GetWindowText(InputCStr);
	m_writeEdit.GetWindowText(OutputCStr);

	string InputStr = CStringTransformationstring(InputCStr);
	string OutputStr = CStringTransformationstring(OutputCStr);

	// TODO: 在此添加控件通知处理程序代码
	/*CRxDbDatabase*pDb = new CRxDbDatabase(false,true);*/
	


	//IDC_WriteEDIT
	if (!dirExists(InputStr))
	{
		AfxMessageBox(_T("输入文件夹不存在！"));
		return;
	}

	if (!dirExists(OutputStr))
	{
		AfxMessageBox(_T("输出文件夹不存在！"));
		return ;
	}


	if (OutputStr.compare(InputStr) == 0) 
	{
		if (AfxMessageBox(_T("源文件夹于目标文件夹相同，是否覆盖？)"), MB_OKCANCEL) == 1)
		{

		}
		else
		{
			return;
		}
	}
	if (m_DataGrid.GetRowCount() <= 2) 
	{
		AfxMessageBox(_T("请输入块属性名！"));
		return;
	}


	bool isAttributes = true;


	for (int i = 1; i < m_DataGrid.GetRowCount()-1; i++)
	{
		string AttributeName = GetCellValue(&m_DataGrid, i, 2);
		if (AttributeName.empty())
		{
			isAttributes = false;
		}
	}
	if (!isAttributes)
	{
		AfxMessageBox(_T("请输入块属性名！"));
		return;
	}

	
	/*pDb->readExbFile(stringTransformationCString(InputStr));



	

	SetBlockAttributeValue(pDb, GetCellValue(&m_DataGrid, 1, 2), GetCellValue(&m_DataGrid,1,3));
	pDb->saveAs(stringTransformationCString(OutputStr));
	delete pDb;*/

	//replaceSave(InputStr, OutputStr);

	std::list< PDbProcessingResults> pDbProcessingResultsList =   ReplaceList(InputStr, OutputStr, &m_DataGrid,&m_progress);

	//AfxMessageBox(stringTransformationCString(PDbProcessingResultsListToString(PDbProcessingResultsList)));

	//打开信息显示窗口并传参
	OpenInfoShowWindows(pDbProcessingResultsList);
	

	// 防止资源冲突

	// 弹出主窗口对话框

	//m_pMainWnd = &infoShowWindows;
	//dlg.DoModal();




}




void ChangeWindows::OnBnClickedWritefolderbutton()
{
	// TODO: 在此添加控件通知处理程序代码
	SetDlgItemText(IDC_WriteEDIT, FicowGetDirectory());
}
void  SetSerialNumber(CGridCtrl* my_CGridCtrl)
{
	int Rows = my_CGridCtrl->GetRowCount();
	
	CString RowName;

	for (int i = 1; i < Rows; i++) 
	{
		
		
		RowName.Format(_T("%d"), i);
	/*	int InsRow = my_CGridCtrl->InsertRow(RowName);*/

		my_CGridCtrl->SetItemText(i, 0, RowName);//设置内容
	}

}


void DeleteRows(CGridCtrl* my_CGridCtrl)
{
	CCellRange Selection = my_CGridCtrl->GetSelectedCellRange();
	if (!my_CGridCtrl->IsValid(Selection))
		return; 
//	CGridCellBase *pCell;

	/*for (int row = Selection.GetMinRow(); row <= Selection.GetMaxRow(); row++)
	{

		CString RowName;
		RowName.Format(_T("%d"), row);
		AfxMessageBox(_T("选中行：")+ RowName);

		for (int col = Selection.GetMinCol(); col <= Selection.GetMaxCol(); col++)
		{
			CString ColName;
			ColName.Format(_T("%d"), col);
			AfxMessageBox(_T("选中列：") + ColName);

			pCell = my_CGridCtrl->GetCell(row, col);
			if (pCell && (pCell->GetState() & GVIS_SELECTED))
			{

				CString deRowName;
				deRowName.Format(_T("%d"), row);
				AfxMessageBox(_T("删除选中行：") + deRowName);
				my_CGridCtrl->DeleteRow(row);
			}
		}
	}*/


	for (int row = Selection.GetMinRow(); row <= Selection.GetMaxRow(); row++)
	{

		/*CString RowName;
		RowName.Format(_T("%d"), row);
		AfxMessageBox(_T("选中行：") + RowName);*/

	/*	CString deRowName;
		deRowName.Format(_T("%d"), row);
		AfxMessageBox(_T("删除选中行：") + deRowName);*/
		my_CGridCtrl->DeleteRow(row);
			
			
	}
	if (my_CGridCtrl->GetRowCount() <= 1) 
	{
		InsertMyNewRow(my_CGridCtrl,30);
	}

	SetSerialNumber(my_CGridCtrl);
	my_CGridCtrl->RedrawWindow();
		
}

	void ChangeWindows::OnBnClickedButton2()
	{
		// TODO: 在此添加控件通知处理程序代码
		DeleteRows(&m_DataGrid);
	}
