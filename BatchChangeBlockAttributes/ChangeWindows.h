#pragma once
#include "afxcmn.h"
#include "CMyEdit.h"
#include "GridCtrl.h"
#include "GridCtrl.h"


// ChangeWindows 对话框

class ChangeWindows : public CDialog
{
	DECLARE_DYNAMIC(ChangeWindows)
	

public:
	ChangeWindows(CWnd* pParent = NULL  );   // 标准构造函数
	
	virtual ~ChangeWindows();


	virtual void OnFinalRelease();


// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CHANGEWINDOWS };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
public:
	// 控件读取的值
	CString m_ReadFolderStr;
	afx_msg void OnBnClickedReadfolderbutton();
	virtual BOOL OnInitDialog();
	afx_msg void OnDropFiles(HDROP hDropInfo);
	CMyEdit m_ReadEdit;
	CMyEdit m_writeEdit;
	
	CGridCtrl m_Grid;
	CGridCtrl m_DataGrid;
	
	//	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	void OnGridEndEdit(NMHDR * pNotifyStruct, LRESULT * pResult);
	
	

	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedWritefolderbutton();

	// 进度条变量
	CProgressCtrl m_progress;
	afx_msg void OnBnClickedButton2();
	
};
// RowHeight默认30
int InsertMyNewRow(CGridCtrl* my_CGridCtrl, int RowHeight=30);
string GetCellValue(CGridCtrl* my_CGridCtrl, int Row, int Col);