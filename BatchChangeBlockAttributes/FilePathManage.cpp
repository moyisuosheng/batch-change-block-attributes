#include "stdafx.h"
#include "FilePathManage.h"
//#include <iostream>
//#include <string>
//#include <io.h>
//using namespace std;
#include <direct.h>
#include "afxwin.h"
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
using namespace std;

FilePathManage::FilePathManage()
{
}


FilePathManage::~FilePathManage()
{
}



void getFilesAll(string path, vector <std::string>& files)
{
	//文件句柄 
	intptr_t  hFile = 0;
	//文件信息 
	struct _finddata_t fileinfo;
	string p;
	if ((hFile = (intptr_t)_findfirst(p.assign(path).append("\\*").c_str(), &fileinfo)) != -1)
	{
		do
		{
			if ((fileinfo.attrib & _A_SUBDIR))
			{
				if (strcmp(fileinfo.name, ".") != 0 && strcmp(fileinfo.name, "..") != 0)
				{
					//files.push_back(p.assign(path).append("\\").append(fileinfo.name) );
					getFilesAll(p.assign(path).append("\\").append(fileinfo.name), files);
				}
			}
			else
			{
				files.push_back(p.assign(path).append("\\").append(fileinfo.name));
			}
		} while (_findnext(hFile, &fileinfo) == 0);
		_findclose(hFile);
	}
}
//获取文件集合
vector <std::string> GetFullNames(string path)
{

	vector <std::string> fullNames;
	getFilesAll(path, fullNames);

	return fullNames;

	/*for (size_t i = 0; i < fullNames.size(); i++)
	{
	std::cout << fullNames[i] + " \n";

	}*/



}

//获取文件所在文件夹
string GetFileFolder(string FullPath)
{
	/*if (GetFileAttributesW(stringTransformationCString(FullPath))&FILE_ATTRIBUTE_DIRECTORY) 
	{
		int ps = (int)FullPath.find_last_of("\\");
		int length = (int)FullPath.length();
		if (ps!=(length-1))
		{

			return FullPath + "\\";
		}
		else
		{
			return FullPath;
		}
		
	}*/

	if (IsFile(FullPath)) 
	{
		int ps = (int)FullPath.find_last_of("\\");

		int length = (int)FullPath.length();
		//文件名无后缀
		string Path = FullPath.substr(0, ps + 1);



		return Path;
	}
	else
	{
		int ps = (int)FullPath.find_last_of("\\");
		int length = (int)FullPath.length();
		if (ps != (length - 1))
		{

			return FullPath + "\\";
		}
		else
		{
			return FullPath;
		}
	}




	
}
//仅仅获取名字，无后缀
string GetFileName(string FullPath) 
{
	int ps = (int)FullPath.find_last_of("\\");
	int pe = (int)FullPath.find_last_of(".");
	int length = (int)FullPath.length();
	//文件名无后缀
	string Name = FullPath.substr(ps + 1, pe - ps + 1);//pic_name="12345"

	return Name;
}

//获取文件夹下的相对路径（含文件名后缀）
string GetFileRelativeFolderFullNameStr(string SaveFolder,string FullPath)
{

	if (((int)FullPath.find(SaveFolder))==0)
	{
		int SaveFolderLength= (int)SaveFolder.length();

		int length = (int)FullPath.length();

		//文件名无后缀

		string FullName = FullPath.substr(SaveFolderLength+1, length - SaveFolderLength-1);//pic_name="12345"
					
		return FullName;
	}
	
	else
	{
		return SaveFolder;
	}

	
}
//仅仅获取名字，有后缀
string GetFileFullName(string FullPath)
{
	int ps = (int)FullPath.find_last_of("\\");
	int length = (int)FullPath.length();
	//文件名无后缀
	
	string FullName = FullPath.substr(ps + 1, length - ps - 1);//pic_name="12345"
													   //路径文件夹
	
	return FullName;
}

//后缀带.
string GetFileDotSuffix(string FullPath)
{
	
	int pe = (int)FullPath.find_last_of(".");
	int length = (int)FullPath.length();

	//后缀带.
	string Suffix = FullPath.substr(pe, length - pe);//pic_name="12345"	
													 //保存地址
	return Suffix;
}
//后缀无.
string GetFileSuffix(string FullPath)
{
	
	int pe = (int)FullPath.find_last_of(".");
	int length = (int)FullPath.length();

	//后缀带.
	string Suffix = FullPath.substr(pe+1, length - pe-1);//pic_name="12345"	
													 //保存地址
	return Suffix;
}
//CString转string
string CStringTransformationstring(CString vs)
{
	
	string str;
	str = CT2CA(vs);//第一种
	//str = vs.GetString();//第二种
	return str;
}

//string转CString
CString stringTransformationCString(string vs)
{
	CString Cstr(vs.c_str());//第一种
	//Cstr.Format(_T("%s"), vs.c_str());//第二种
	return Cstr;
}
//判断文件夹是否存在
bool dirExists(const std::string& dirName_in)
{
	int ftyp = _access(dirName_in.c_str(), 0);

	if (0 == ftyp)
		return true;   // this is a directory!
	else
		return false;    // this is not a directory!
}

//目录是否存在，也可判断一个路径是否为文件夹
BOOL FolderExists(CString strFolder)
{
	DWORD attr;
	attr = GetFileAttributes(strFolder);
	return (attr != (DWORD)(-1)) && (attr & FILE_ATTRIBUTE_DIRECTORY);
}

//创建目录，可以多层创建；创建成功或者已存在，返回true; 否则返回false
BOOL CreateMtlDir(CString strPath)
{
	int nLen = strPath.GetLength();
	if (nLen < 2)
	{
		return FALSE;  //如果字符串长度小于，则不是有效的路径格式。
	}

	if (_T('\\') == strPath[nLen - 1])//删除末尾的"\\"
	{
		strPath = strPath.Left(nLen - 1);
		nLen = strPath.GetLength();
	}

	if (nLen <= 0)
	{
		return FALSE;
	}

	if (nLen <= 3) //如果长度<=3，那么必定是根目录格式
	{
		if (FolderExists(strPath))
		{
			return TRUE;//如果根目录存在,返回true;
		}
		else
		{
			return FALSE;//如果不存在，根目录是无法创建的，返回失败；
		}
	}

	if (FolderExists(strPath))
	{
		return TRUE;//如果目录存在,返回true;
	}

	// 分开父目录和本身目录名称
	CString Parent;
	Parent = strPath.Left(strPath.ReverseFind(_T('\\')));

	if (Parent.GetLength() <= 0)
	{
		return FALSE; //目录名称错误
	}

	BOOL Ret = CreateMtlDir(Parent);  //递归创建父目录

	if (Ret)      // 父目录存在,直接创建目录
	{
		SECURITY_ATTRIBUTES sa;
		sa.nLength = sizeof(SECURITY_ATTRIBUTES);
		sa.lpSecurityDescriptor = NULL;
		sa.bInheritHandle = 0;
		Ret = (CreateDirectory(strPath, &sa) == TRUE);
		return Ret;
	}
	else
	{
		return FALSE;
	}
}
/*LPCTSTR不是一个类型，而是两种类型：LPCSTR和LPCWSTR其中之一。会根据你当前程序是否使用UNICODE字符集来变成那二者之一。如果使用UNICODE字符集，则LPCTSTR = LPCWSTR，否则LPCTSTR = LPCSTR。
标准库的std::string转换成LPCSTR很简单：直接调用c_str()即可。例：
std::string a = "abc";
LPCSTR str = a.c_str();
标准库还有一个wstring，代表宽字符的string，std::wstring转换成LPCWSTR也一样很简单：
std::wstring a = L"abc";
LPCWSTR str = a.c_str();
如果要是std::string转换成LPCWSTR或者std::wstring转换成LPCSTR那就比较麻烦了，需要调用MultiByteToWideChar或WideCharToMultiByte进行字符集之间的转换。不过大多数时候不需要这种交叉转换，一个程序一般只会使用一种字符集。
*/

//是文件返回true
bool IsFile(string FullPath) 
{
	WIN32_FIND_DATAA FindFileData;
	FindFirstFileA(FullPath.c_str(), &FindFileData);
	//FindFirstFileA(tempPath,&FindFileData);
	if (FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
	{
		//Folder
		return false;
	}
	else
	{
		//File
		return true;
		
	}
}

//TCHAR转string
std::string TCHAR2STRING(TCHAR* STR)
{
	int iLen = WideCharToMultiByte(CP_ACP, 0,STR, -1, NULL, 0, NULL, NULL);

char* chRtn = new char[iLen * sizeof(char)];

WideCharToMultiByte(CP_ACP, 0, STR, -1, chRtn, iLen, NULL, NULL);

std::string str(chRtn);

return str;

}