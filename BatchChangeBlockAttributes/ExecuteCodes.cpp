
#include "stdafx.h"
#include "crxDefine.h"
#include "crxutcds.h"
#include "dbmain.h"
#include "crxdocman.h"
#include "dbsymtb.h"
#include "dbapserv.h"
#include "cdscodes.h"
#include "crxedcds.h"
#include "dbbomsymtb.h"
#include "dbapserv.h"
#include "dbsymtb.h"
#include "resource.h"
#include "crxDefine.h"
#include "crxutcds.h"
#include "dbmain.h"
#include "crxdocman.h"
#include "dbsymtb.h"
#include "dbapserv.h"
#include "cdscodes.h"
#include "crxedcds.h"
#include "dbbomsymtb.h"
#include "dbtrans.h"


#include <ctime>

#include "stdafx.h"
#include "resource.h"
#include "ChangeWindows.h"

#include "ExecuteCodes.h"
#include "GridCellCombo.h"
#include "GridCellCheck.h"
#include "GridCellNumeric.h"
#include "GridCellDateTime.h"

#include "FilePathManage.h"
#include <iostream>
#include <string>
#include <io.h>
#include <direct.h>
#include <iostream>

#include "ChangeWindows.h"
#include "ShowInfoWindows.h"

//-----------------------------------------------------------------------------
#define szRDS _RXST("");

using namespace std;


//设置对应图纸的单个属性值
AttributeStruct SetBlockAttributeValue( CRxDbDatabase  *pDb, AttributeStruct attribute){


	CDraft::ErrorStatus es = CDraft::eOk;

	CRxDbPaperTable* pTable;

	es = pDb->getPaperTable(pTable, CRxDb::kForRead);

	if (es != CDraft::eOk)
	{
		attribute.ProcessingResults = EnumProcessingResults::TableRecordsMissing;

		//符号表不存在
		return  attribute;
		
	}
		

	CRxDbPaperTableRecord* pRecord;

	pTable->getAt(_T("Model"), pRecord, CRxDb::kForRead);

	if (pRecord != NULL)
	{

		CString SetstrAttName= attribute.Name.c_str();
		CString SetstrAttValue= attribute.Value.c_str();

		CString GetstrAttName = attribute.Name.c_str();

		CRxAttCollInfoObject* pTitleInfo;

		pRecord->getTitleInfo(&pTitleInfo);

		int iCount = pTitleInfo->getAttributesCount();

		for (int i = 0; i<iCount; i++)

		{
			CxCHAR* cAttName;
			pTitleInfo->getAttributeName(cAttName, i);
			GetstrAttName = cAttName;



			if (GetstrAttName.Compare(SetstrAttName) == 0)
			{
				//设置图纸属性值
				pTitleInfo->setAttributeValue(SetstrAttValue, i);
				break;

			}

		}
		es = pRecord->setTitleInfo(pTitleInfo);

		if (es == CDraft::eOk)
		{
			pRecord->ReleaseAttCollInfoObject(pTitleInfo);//释放
			//设置成功

			attribute.ProcessingResults = EnumProcessingResults::Ok;

			//符号表不存在
			return  attribute;
			
		//	crxutPrintf(_T("已将标题栏设计日期属性设置为当前系统日期！"));

		}
		else
		{
			pRecord->ReleaseAttCollInfoObject(pTitleInfo);//释放
			attribute.ProcessingResults = EnumProcessingResults::Error;
			return  attribute;
		//	crxutPrintf(_T("更新标题栏设计日期属性失败！"));

		}
		
	}
	else
	{
		pRecord->ReleaseAttCollInfoObject(nullptr);//释放
		attribute.ProcessingResults = EnumProcessingResults::TableRecordsMissing;
		return  attribute;
	}

}
///数据筛分处理函数—处理所有的属性数据
 PDbProcessingResults DataProcessing(CRxDbDatabase * pDb, CGridCtrl* my_CGridCtrl)
{
	//AfxMessageBox(_T("进入DataProcessing"));

	//单个图纸处理结果
	PDbProcessingResults pDbProcessingResults;
	
	int Rowindx = my_CGridCtrl->GetRowCount();

	for (int i = 1; i <Rowindx -1; i++)
	{
		string	gridDataTypeVale = GetCellValue(my_CGridCtrl,i,1);
		//第一列为标题栏
		if (gridDataTypeVale.compare("标题栏") == 0)
		{
			//第二列不为空
			
			AttributeStruct attribute;

			//需要更改的属性名
			attribute.Name = GetCellValue(my_CGridCtrl, i, 2);
			//需要更改的属性值
			attribute.Value = GetCellValue(my_CGridCtrl, i,3);
			
			attribute = SetBlockAttributeValue(pDb, attribute);

			
			switch (attribute.ProcessingResults)
			{
				case	EnumProcessingResults::Ok:
					
					if (attribute.Value.empty())
					{
						//当赋值的属性值为空并且返回OK时
						attribute.Info += "属性: " + attribute.Name + " 重置为 空 \r\n";
						break;
					}
					else
					{
						attribute.Info += "属性: " + attribute.Name + " 设置成功\r\n";
						break;
					}
				case	EnumProcessingResults::Error:
					attribute.Info += "属性: " + attribute.Name + " 设置失败\r\n";
					
					break;
				case	EnumProcessingResults::AttributeMissing:
					attribute.Info += "属性: " + attribute.Name + " 未找到\r\n";
					
					break;
				case	EnumProcessingResults::TableRecordsMissing:
					attribute.Info += "属性: " + attribute.Name + " 不存在（请检查标题栏）\r\n";
				
					break;
				default:
					attribute.Info += "属性: " + attribute.Name + " 情况未知 \r\n";
					break;
				}
			//将元素添加到列表末尾
			pDbProcessingResults.attributeList.push_back(attribute);

		}

		
	}

	//TODO 断点
	//AfxMessageBox(stringTransformationCString(pDbProcessingResults.Info));

	return pDbProcessingResults;
}

//处理单个文件
 PDbProcessingResults replaceSave(string readFileFull, string readFolderPath, string writeFolderPath, CGridCtrl* my_CGridCtrl)
{
//	AfxMessageBox(_T("进入replaceSave"));
		//单个文件返回数据
		PDbProcessingResults pDbProcessingResults;
	
		CRxDbDatabase*pDb = new CRxDbDatabase(false, true);
	
		string readFileFullPath = GetFileFolder(readFileFull);
	//	AfxMessageBox(_T("读取文件夹") + stringTransformationCString(readFileFullPath));

		string FileSuffix = GetFileSuffix(readFileFull);
//		AfxMessageBox(_T("读取文件后缀") + stringTransformationCString(FileSuffix));

		//获取含后缀文件名
		string FileFullName = GetFileFullName(readFileFull);
		//获取不含后缀文件名
		string FileDotSuffix = GetFileDotSuffix(readFileFull);
	//	AfxMessageBox(_T("读取文件后缀FileDotSuffix") + stringTransformationCString(FileDotSuffix));

		string	FileSaveFullPath = GetFileFolder(writeFolderPath) + GetFileRelativeFolderFullNameStr(readFolderPath,readFileFull);
//		AfxMessageBox(_T("文件保存全名") + stringTransformationCString(FileSaveFullPath));

		string	FileSavefolder = GetFileFolder(FileSaveFullPath);
//		AfxMessageBox(_T("文件保存路径") + stringTransformationCString(FileSavefolder));
		if (CreateMtlDir(stringTransformationCString(FileSavefolder)))
		{
			//创建目录，可以多层创建；创建成功或者已存在，返回true; 否则返回false
			

			// if this folder not exist, create a new one.
		//	mkdir(FileSavefolder.c_str());   // 返回 0 表示创建成功，-1 表示失败
										 //换成 ::_mkdir  ::_access 也行，不知道什么意思
		}

		
		


		if (FileSuffix.compare("exb") == 0)
		{
		
			pDb->readExbFile(stringTransformationCString(readFileFull));

			/*CRxApDocument* doc = crxDocManager->document(pDb);
			CRxApDocument::SaveFormat FileSaveFormat = doc->formatForSave();*/
			



			pDbProcessingResults = DataProcessing( pDb, my_CGridCtrl);

			pDbProcessingResults.FileSavePath = FileSaveFullPath;
			pDbProcessingResults.FileFullName = FileFullName;
			pDbProcessingResults.FileReadPath = readFileFull;
		
			//TODO 断点 显示返回信息
			//AfxMessageBox( stringTransformationCString(pDbProcessingResults.Info));
			
			pDb->saveAs(stringTransformationCString(FileSaveFullPath),true, CRxDb::kEXB_CURRENT);
			
			
		}
		else if (FileSuffix.compare("dwg") == 0)
		{
		
			pDb->readDwgFile(stringTransformationCString(readFileFull));
		/*	CRxApDocument* doc = crxDocManager->document(pDb);
			CRxApDocument::SaveFormat FileSaveFormat = doc->formatForSave();*/

			/*CRxApDocument::SaveFormat*/

			pDbProcessingResults = DataProcessing(pDb, my_CGridCtrl);

			pDbProcessingResults.FileSavePath = FileSaveFullPath;
			pDbProcessingResults.FileFullName = FileFullName;
			pDbProcessingResults.FileReadPath = readFileFull;

			//TODO 断点 显示返回信息
		//	AfxMessageBox(stringTransformationCString(pDbProcessingResults.Info));


			pDb->saveAs(stringTransformationCString(FileSaveFullPath), true, CRxDb::kDHL_1021);
		//pDb->saveAs(stringTransformationCString(FileSaveFullPath),false, CRxDb::kDHL_1021);

			
		}
		else
		{
			
		}

		//DataProcessing(pDb, dt);

		


	
	//	AfxMessageBox(_T("退出replace"));
	delete pDb;

	return pDbProcessingResults;
}

///替换—处理多个文件
//单个文件返回数据
std::list< PDbProcessingResults> ReplaceList(string readFolderPath,string writeFolderPath, CGridCtrl* my_CGridCtrl, CProgressCtrl * my_CProgressCtrl)
{
//	AfxMessageBox(_T("进入replace"));

	list< PDbProcessingResults>  pDbProcessingResultsList;

	//获取文件集合
	vector <std::string> readFileFullPaths = GetFullNames(readFolderPath);
//	AfxMessageBox(_T("得到文件集合"));

	//获取文件数量
	size_t FileNum = readFileFullPaths.size();

	my_CProgressCtrl->SetRange(0,short (FileNum-1));
	my_CProgressCtrl->SetPos(0);
	
//	my_CProgressCtrl->SetRange(0,4);

	for (size_t i = 0; i < FileNum; i++)
	{
	//	AfxMessageBox(_T("读取的文件全名") + stringTransformationCString(readFileFullPaths[i]));

		
		PDbProcessingResults pDbProcessingResults =	replaceSave(readFileFullPaths[i], readFolderPath, writeFolderPath, my_CGridCtrl);

		pDbProcessingResultsList.push_back(pDbProcessingResults);


		my_CProgressCtrl->SetPos((int)i);
	//	my_CProgressCtrl->StepIt();
	}



	//AfxMessageBox(_T("转换完成！"));
	my_CProgressCtrl->SetPos(0);
	readFileFullPaths.clear();
	return  pDbProcessingResultsList;
}

void ReadFileData(string Path)
{

}
void CRXReadEXB(CRxDbDatabase* pDb,string Path)
{
	//AfxMessageBox(_T("进入CRXReadEXB"));

	// 使用kFalse作为构造函数的参数，创建一个空的图形数据库
	// 这样保证图形数据库仅仅包含读入的内容
//	CRxDbDatabase*pDb = new CRxDbDatabase(CAXA::kFalse);
	// CRxDbDatabase::readDwgFile()函数可以自动添加dwg扩展名
	
	
	pDb->readExbFile(stringTransformationCString(Path));
	
//	delete pDb;
}
void CRXReadDWG(CRxDbDatabase* pDb, string Path)
{
	//AfxMessageBox(_T("进入CRXReadEXB"));
	
	// 使用kFalse作为构造函数的参数，创建一个空的图形数据库
	// 这样保证图形数据库仅仅包含读入的内容
	//	CRxDbDatabase*pDb = new CRxDbDatabase(CAXA::kFalse);
	// CRxDbDatabase::readDwgFile()函数可以自动添加dwg扩展名
	
	pDb->readDwgFile(stringTransformationCString(Path));

	//	delete pDb;
}//OpenChangeWindows
void OpenChangeWindows()

{

	// 防止资源冲突

	CCrxModuleResourceOverride resOverride;

	// 显示ObjectCRX的模态对话框

	ChangeWindows theDialog;


	if (theDialog.DoModal() == IDOK)

	{

		AfxMessageBox(_T("关闭对话框！"));

	}

}
//打开信息显示窗口并传参
bool OpenInfoShowWindows(std::list< PDbProcessingResults> pDbProcessingResultsList)
{
	// 防止资源冲突

	CCrxModuleResourceOverride resOverride;
	
	//CDialog dlg(IDD_SHOWINFOWINDOWS);
	ShowInfoWindows showInfoWindows;
	showInfoWindows.pDbProcessingResultsList = pDbProcessingResultsList;
	if (showInfoWindows.DoModal() == IDOK)
	{
		//AfxMessageBox(_T("关闭对话框！"));
	}
	
	
//	ShowInfoWindows *showInfoWindows = new ShowInfoWindows; //非模态



	//showInfoWindows->DoModal();
//	showInfoWindows->Create(IDD_SHOWINFOWINDOWS, nullptr); //第一个参数是对话框ID号
//	showInfoWindows->ShowWindow(SW_SHOW);
	
	return true;

}


string PDbProcessingResultsListToString(std::list< PDbProcessingResults> pDbProcessingResultsList) {
	string infoStr;

	for (std::list<PDbProcessingResults>::iterator it = pDbProcessingResultsList.begin(); it != pDbProcessingResultsList.end(); ++it) {


		PDbProcessingResults pDbProcessingResults = *it;

		for (std::list<AttributeStruct>::iterator itAttributeStruct = pDbProcessingResults.attributeList.begin(); itAttributeStruct != pDbProcessingResults.attributeList.end(); ++itAttributeStruct) {


			AttributeStruct attributeStruct = *itAttributeStruct;

			infoStr += "文件：" + pDbProcessingResults.FileFullName + " 描述：" + attributeStruct.Info + 
				" 保存路径：" + pDbProcessingResults.FileSavePath + " 读取路径：" + pDbProcessingResults.FileReadPath + "\r\n";


		}


	


	}
	return infoStr;
}

string GetCellValue(CGridCtrl* my_CGridCtrl, int Row, int Col)
{
	my_CGridCtrl->CopyTextFromGrid();
	CGridCellBase *pCell;
	pCell = my_CGridCtrl->GetCell(Row, Col);


	// if (!pCell->GetText())
	//    str += _T(" ");
	// else 
	CString	 Getvalue = (CString)pCell->GetText();



	//	 CString myvalue;
	//	 myvalue.Format(_T("行：%d 列：%d 值：%s"), Row, Col,Getvalue);
	//	 AfxMessageBox(myvalue);

	return CStringTransformationstring(Getvalue);
}
