// BatchChangeBlockAttributesEntryPoint.cpp : CBatchChangeBlockAttributesEntryPoint 

#include "stdafx.h"
#include "resource.h"
#include "ExecuteCodes.h"
#include "CrxCusUI.h"
//-----------------------------------------------------------------------------
#define szRDS _RXST("")
#pragma comment(lib,"CrxUi.lib")





void CRXBatchChangeBlockAttributesUI()

{

	//CRxMenuBar* pMenuBar = crxUIManager->getCRxMenuBar();  //拿到主菜单对象

	//if (pMenuBar)

	//{

	//	unsigned int count = pMenuBar->getCount(); //获得子菜单个数

	//	CRxPopupMenu* pNewMenu = pMenuBar->addPopupMenu(7, _T("编码名称另存菜单")); //添加一个叫”Test”的子菜单



	//	pNewMenu->addMenuItem(count, _T("编码名称另存菜单"), _T("GHello2"));  //添加一个菜单项，关联GHello命令

	//	pNewMenu->addSeparator(1);                            //顺序添加一个分隔符

	//	pNewMenu->addSubMenu(2, _T("Sub"));                  //顺序添加一个子菜单，返回的也是CRxPopupMenu对象，可以递归添加
	//	crxutPrintf(_T("\n已获得主菜单%u"), count);
	//}
	//else
	//{
	//	crxutPrintf(_T("\n未获得主菜单"));
	//}


	CRxToolBars* pToolbars = crxUIManager->getCRxToolBars();  //获得Toolbars对象

	if (pToolbars)

	{

		CRxToobar* pToolbar = pToolbars->addCRxToolBar(_T("CRXBatchChangeBlockAttributesUI"), _T("批量更改"));   //添加一个叫” UITestBar”的Toolbar，第一个参数为一个唯一标示字符串，可以自己定义
		/*pToolbars->removeCRxToolBar(pToolbar);
		 pToolbar = pToolbars->addCRxToolBar(_T("CRXBatchChangeBlockAttributesUI"), _T("批量更改"));*/
		unsigned int conut = pToolbar->getCount();

		for (unsigned int i = 0; i < conut; i++)
		{
			//	removeCRxToolBar(i);


			pToolbar->RemoveCRxToolbarItem(i);
		}
		//conut = pToolbar->getCount();
		

		pToolbar->addCRxToolBarButton(0, _T("批量更改(BCShow)"), _T("批量更改"), _T("BatchChangeBlockAttributes"));            //添加一个叫Test1的Button关联GHello命令
		
		//pToolbar->addSeparator(1);                                                                  //顺序添加一个分隔符

		//pToolbar->addCRxToolBarButton(2, _T("Test2"), _T("Test2"), _T("GHello2"));

		//	pToolbar->RemoveCRxToolbarItem(0);

		pToolbar->setVisible();
	}



}

void CRXBatchChangeBlockAttributesUIDelete()

{

	CRxToolBars* pToolbars = crxUIManager->getCRxToolBars();  //获得Toolbars对象

	if (pToolbars)

	{
		pToolbars->removeCRxToolBar(_T("CRXBatchChangeBlockAttributesUI"));
																												
	}



}


//-----------------------------------------------------------------------------
//----- CBatchChangeBlockAttributesEntryPoint
class CBatchChangeBlockAttributesApp:public AcRxArxApp
{
public:
	CBatchChangeBlockAttributesApp():AcRxArxApp()
	{
	}

	virtual AcRx::AppRetCode On_kInitAppMsg(void *pkt) 
	{
		// TODO: Load dependencies here

		// You *must* call On_kInitAppMsg here
		AcRx::AppRetCode retCode =AcRxArxApp::On_kInitAppMsg(pkt);

		// TODO: Add your initialization code here

		// 注册命令
	//	crxedRegCmds->addCommand(_T("BatchChangeBlockAttributesApp"), _T("BatchChangeBlockAttributes"), _T("ChangeBlockAttribute"), ACRX_CMD_MODAL, &OpenChangeWindows);

		crxedRegCmds->addCommand(_T("BatchChangeBlockAttributesApp"), _T("BatchChangeBlockAttributes"), _T("BCShow"), ACRX_CMD_MODAL, &OpenChangeWindows);
		crxedRegCmds->addCommand(_T("BatchChangeBlockAttributesAppUI"), _T("BatchChangeBlockAttributesUI"), _T("BCShowUI"), ACRX_CMD_MODAL, &CRXBatchChangeBlockAttributesUI);
		CRXBatchChangeBlockAttributesUI();
		return (retCode) ;
	}

	virtual AcRx::AppRetCode On_kUnloadAppMsg(void *pkt)
	{
		// TODO: Add your code here

		// You *must* call On_kUnloadAppMsg here
		AcRx::AppRetCode retCode =AcRxArxApp::On_kUnloadAppMsg(pkt);		

		// TODO: Unload dependencies here
		// TODO: 卸载命令

		crxedRegCmds->removeGroup(_T("BatchChangeBlockAttributesApp"));
		crxedRegCmds->removeGroup(_T("BatchChangeBlockAttributesAppUI"));
		CRXBatchChangeBlockAttributesUIDelete();
		return (retCode);
	}

	virtual void RegisterServerComponents()
	{

	}
};

IMPLEMENT_ARX_ENTRYPOINT(CBatchChangeBlockAttributesApp)