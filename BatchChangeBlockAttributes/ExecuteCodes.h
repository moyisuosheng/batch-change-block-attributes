#pragma once
#include "GridCellCombo.h"
#include "GridCellCheck.h"
#include "GridCellNumeric.h"
#include "GridCellDateTime.h"




AttributeStruct SetBlockAttributeValue(CRxDbDatabase  *pDb, AttributeStruct attribute);
void OpenChangeWindows();

bool OpenInfoShowWindows(std::list< PDbProcessingResults> pDbProcessingResultsList);

void CRXReadEXB(CRxDbDatabase* pDb, string Path);
void CRXReadDWG(CRxDbDatabase* pDb, string Path);
//void replace(string readFolderPath, string writeFolderPath, CGridCtrl* my_CGridCtrl);
//void DataProcessing(CRxDbDatabase * pDb, DataTable dt);
///另存单个文件
std::list< PDbProcessingResults> ReplaceList(string readFolderPath, string writeFolderPath, CGridCtrl* my_CGridCtrl, CProgressCtrl * my_CProgressCtrl);
string PDbProcessingResultsListToString(std::list< PDbProcessingResults> pDbProcessingResultsList);
//获取单元格值
string GetCellValue(CGridCtrl* my_CGridCtrl, int Row, int Col);
