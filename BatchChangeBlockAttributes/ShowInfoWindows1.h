#pragma once
#include "afxcmn.h"
#include "CMyEdit.h"
#include "GridCtrl.h"
#include "GridCtrl.h"

// ShowInfoWindows 对话框

class ShowInfoWindows : public CDialog
{
	DECLARE_DYNAMIC(ShowInfoWindows)

public:
	ShowInfoWindows(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~ShowInfoWindows();
	std::list< PDbProcessingResults> pDbProcessingResultsList;

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SHOWINFOWINDOWS };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:

	

	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
	virtual void OnFinalRelease();

	CGridCtrl m_ShowGrid;
	
};
