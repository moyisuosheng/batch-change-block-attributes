//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 BatchChangeBlockAttributes.rc 使用
//
#define IDS_PROJNAME                    100
#define IDD_CHANGEWINDOWS               103
#define IDD_SHOWINFOWINDOWS             105
#define IDC_ReadMFCEDITBROWSE1          1001
#define IDC_ReadFolderBUTTON            1004
#define IDC_WriteFolderBUTTON           1005
#define IDC_ReadEDIT1                   1006
#define IDC_ReadEDIT                    1006
#define IDC_WriteEDIT                   1007
#define IDC_GRID                        1011
#define IDC_BUTTON3                     1014
#define IDC_BUTTON1                     1015
#define IDC_ShowPROGRESS                1016
#define IDC_BUTTON2                     1017

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        109
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1021
#define _APS_NEXT_SYMED_VALUE           106
#endif
#endif
