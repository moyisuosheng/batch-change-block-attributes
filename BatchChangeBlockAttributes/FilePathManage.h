#pragma once
#include<vector>
#include<list>
#include <string>
#include "fstream"
#include "io.h"
#include <iostream>
class FilePathManage
{
public:
	FilePathManage();
	~FilePathManage();
};

vector <std::string> GetFullNames(string path);
//获取文件所在文件夹
string GetFileFolder(string FullPath);
//仅仅获取名字，无后缀
string GetFileName(string FullPath);
string GetFileFullName(string FullPath);
//获取文件夹下的相对路径（含文件名后缀）
string GetFileRelativeFolderFullNameStr(string SaveFolder, string FullPath);
//后缀带.
string GetFileDotSuffix(string FullPath);
//后缀无.
string GetFileSuffix(string FullPath);
//CString转string
string CStringTransformationstring(CString vs);

//string转CString
CString stringTransformationCString(string vs);
//判断文件夹是否存在
bool dirExists(const std::string& dirName_in);
//目录是否存在，也可判断一个路径是否为文件夹
BOOL FolderExists(CString strFolder);
//创建目录，可以多层创建；创建成功或者已存在，返回true; 否则返回false
BOOL CreateMtlDir(CString strPath);
//是文件返回true
bool IsFile(string FullPath);

//TCHAR转string
std::string TCHAR2STRING(TCHAR* STR);